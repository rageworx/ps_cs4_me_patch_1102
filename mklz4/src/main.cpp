#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include <lz4.h>

using namespace std;

string srcfn;
string dstfn;

bool compress()
{
    FILE* srcf = fopen( srcfn.c_str(), "rb" );
    if ( srcf != NULL )
    {
        fseek( srcf, 0, SEEK_END );
        unsigned srcfsz = ftell( srcf );
        rewind( srcf );

        if ( srcfsz == 0 )
        {
            fclose( srcf );
            return false;
        }

        char* rbuff = new char[ srcfsz ];
        if ( rbuff == NULL )
        {
            fclose( srcf );
            return false;
        }

        fread( rbuff, 1, srcfsz, srcf );
        fclose( srcf );

        char* wbuff = new char[ srcfsz + 4 ];
        if ( wbuff == NULL )
        {
            delete[] rbuff;
            return false;
        }

        int csz = \
        LZ4_compress_default( rbuff, &wbuff[4], srcfsz, srcfsz );

        delete[] rbuff;

        if ( csz > 0 )
        {
            int* psz = (int*)wbuff;
            *psz = srcfsz;

            FILE* dstf = fopen( dstfn.c_str(), "wb" );
            if ( dstf != NULL )
            {
                fwrite( wbuff, 1, csz + 4, dstf );
                fclose( dstf );

                printf( "compress size = %d + 4 :",
                        csz );

                delete[] wbuff;
                return true;
            }
        }

        delete[] wbuff;
    }

    return false;
}

int main( int argc, char** argv )
{
    if ( argc == 1 )
    {
        printf( "need parameters.\n" );
        return 0;
    }

    if ( argc > 1 )
    {
        srcfn = argv[1];
    }

    if ( argc > 2 )
    {
        dstfn = argv[2];
    }

    if ( dstfn.size() == 0 )
    {
        dstfn = srcfn + ".lz4";
    }

    printf( "source: %s\n", srcfn.c_str() );
    printf( "output: %s\n", dstfn.c_str() );

    if ( compress() == true )
    {
        printf( "successfully compressed.\n" );
    }
    else
    {
        printf( "failed to compress.\n" );
    }
    return 0;
}
