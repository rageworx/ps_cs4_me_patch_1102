# ps_cs4_me_patch_1102

A simple console program PS-CS4(11.0) patch to 11.0.2 for Windows32/64.

## Update history

### 2019-09-11
* Fixed typo errors.
* Failure message for finding PS CS4.

### 2019-09-07
* version 1.1.0.6
* now using LZ4 compression for patches in resource block.

### 2019-08-31
* version 1.0.0.0
* Prototype
