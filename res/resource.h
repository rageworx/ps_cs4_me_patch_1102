#ifndef NOCPUID
#include <cpuid.h>
#endif /// of NOCPUID

#define APP_VERSION					1,1,0,7
#define APP_VERSION_STR				"1.1.0.7"

#define RES_FILEDESC				"PS CS4 11.0.2 patcher for Windows"

#define IDC_ICON_A                  101
