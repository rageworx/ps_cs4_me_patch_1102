#include <windows.h>
#include <shlwapi.h>
#include <unistd.h>
#include <fcntl.h>

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <string>

#include <lz4.h>

using namespace std;

enum {
    CONCOL_BLACK = 0,
    CONCOL_TIL = FOREGROUND_RED,
    CONCOL_GREEN = FOREGROUND_GREEN,
    CONCOL_NAVY = FOREGROUND_BLUE,
    CONCOL_PURPLE = FOREGROUND_BLUE | FOREGROUND_RED,
    CONCOL_SILVER = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN,
    CONCOL_RED = FOREGROUND_RED | FOREGROUND_INTENSITY,
    CONCOL_LIME = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
    CONCOL_YELLOW = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY,
    CONCOL_BLUE = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
    CONCOL_MAGENTA = FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY,
    CONCOL_WHITE = CONCOL_SILVER | FOREGROUND_INTENSITY,
}ConsoleColorTypes;

wstring prgf32;
wstring prgf64;
wstring pscs4_32;
wstring pscs4_64;

HANDLE hConsole;
FILE* hfCon = NULL;

const wchar_t* patch32_list[] = 
{
    L"patch32_1032",
    L"patch32_1050",
    L"patch32_1051",
    L"patch32_1718"
};

const wchar_t* patch64_list[] = 
{
    L"patch64_1031",
    L"patch64_1047",
    L"patch64_1048",
    L"patch64_1736"
};

const wchar_t* dest_list[] =
{
    L"CoolType.dll",
    L"Photoshop.dll",
    L"Photoshop.exe",
    L"Plug-ins\\Filters\\LightingEffects.8BF"
};

void loadProfiles()
{
    prgf32 = _wgetenv( L"ProgramFiles(X86)" );
    prgf64 = _wgetenv( L"ProgramFiles" );

    if ( prgf32.size() > 0 )
    {
        pscs4_32 = prgf32 + L"\\Adobe\\Photoshop CS4";
    }

    if ( prgf64.size() > 0 )
    {
        pscs4_64 = prgf32 + L"\\Adobe\\Photoshop CS4";
    }
}

BOOL IsElevated( ) 
{
    BOOL fRet = FALSE;

    HANDLE hToken = NULL;
    if( OpenProcessToken( GetCurrentProcess(),TOKEN_QUERY,&hToken ) ) 
    {
        TOKEN_ELEVATION Elevation;
        DWORD cbSize = sizeof( TOKEN_ELEVATION );
        if( GetTokenInformation( hToken, TokenElevation, 
                                 &Elevation, sizeof( Elevation ), 
                                 &cbSize ) ) 
        {
            fRet = Elevation.TokenIsElevated;
        }
    }

    if( hToken ) 
    {
        CloseHandle( hToken );
    }

    return fRet;
}

bool getResource( const wchar_t* scheme, unsigned char** buff, unsigned* buffsz )
{
    HRSRC rsrc = FindResource( NULL, scheme, RT_RCDATA );
    if ( rsrc != NULL )
    {
        *buffsz = SizeofResource( NULL, rsrc );
        if ( *buffsz > 0 )
        {
            HGLOBAL glb = LoadResource( NULL, rsrc );
            if ( glb != NULL )
            {
                void* fb = LockResource( glb );
                if ( fb != NULL )
                {
                    unsigned char* cpbuff = new unsigned char[ *buffsz ];
                    if ( cpbuff != NULL )
                    {
                        memcpy( cpbuff, fb, *buffsz );

                        *buff = cpbuff;

                        UnlockResource( glb );
                        return true;
                    }

                    UnlockResource( glb );
                }
            }
        }
    }

    return false;
}

inline void setconcol(int textcol, int backcol = 0)
{
    if ((textcol % 16) == (backcol % 16)) 
        textcol++;

    textcol %= 16; 
    backcol %= 16;
    HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    unsigned short wAttr = ((unsigned)backcol << 4) | (unsigned)textcol;
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    SetConsoleTextAttribute( hStdOut, wAttr );
}

void patchWork()
{
    setconcol( CONCOL_WHITE );
    wprintf( L"Looking for Photoshop CS4 ...\n" );

    wstring patch_path;
    const wchar_t** srclist = NULL;
    
    if ( PathFileExists( pscs4_32.c_str() ) == TRUE )
    {
        patch_path = pscs4_32;
        srclist = patch32_list;
    }
    else
    if ( PathFileExists( pscs4_64.c_str() ) == TRUE )
    {
        patch_path = pscs4_64;
        srclist = patch64_list;
    }
    else
    {
        setconcol( CONCOL_RED );
        wprintf( L"FAILURE.\n" );
        wprintf( L"* Cannot find Photoshop CS4 in Program files.\n" );
        wprintf( L"\n" );
        return;
    }

    if ( patch_path.size() > 0 )
    {
        wprintf( L"- Found path at %S\n", patch_path.c_str() );
        wprintf( L"Checking administrator rights ... " );
        if ( IsElevated() == FALSE )
        {
            setconcol( CONCOL_RED );
            wprintf( L"FAILURE!\n\n" );
            wprintf( L"* You need to run this program in administrator level.\n" );
            wprintf( L"* Runs as administrator for this program again.\n" );
            wprintf( L"\n" );
            return;
        }
        else
        {
            wprintf( L"Ok.\n" );
        }

        setconcol( CONCOL_MAGENTA );

        for( unsigned cnt=0; cnt<4; cnt++ )
        {
            wprintf( L"- Uncompress and patching %S ... ", dest_list[cnt] );
            
            unsigned char* buff = NULL;
            unsigned buffsz = 0;
            
            if ( getResource( srclist[cnt], &buff, &buffsz ) == true )
            {
                wstring fpath = patch_path + L"\\";
                fpath += dest_list[cnt];

                int* psz = (int*)buff;
                int dcbsz = *psz;
                char* dcb = new char[dcbsz];

                if ( dcb == NULL )
                {
                    delete[] buff;
                    setconcol( CONCOL_RED );
                    wprintf( L"- Error:\n"
                             L"--compression buffer cannot allocated.\n" );
                    return;
                }

                memset( dcb, 0, dcbsz );

                int rsz = \
                LZ4_decompress_safe( (const char*)&buff[4],
                                     dcb,
                                     (int)buffsz-4,
                                     (int)dcbsz );

                delete[] buff;

                if ( rsz > 0 )
                {
                    FILE* fp = _wfopen( fpath.c_str(), L"wb" );
                    if ( fp != NULL )
                    {
                        fwrite( dcb, 1, dcbsz, fp );
                        fclose( fp );
                        wprintf( L"Ok.\n" );
                    }
                    else
                    {
                        wprintf( L"Error.\n" );
                        wprintf( L"-- Can not write file %S\n", 
                                 fpath.c_str() );
                        return;
                    }
                }
                else
                {
                    setconcol( CONCOL_RED );
                    wprintf( L"Error.\n-- uncompressing failed with %d.\n",
                             rsz );
                    return;
                }

                delete[] dcb;
            }
            else
            {
                setconcol( CONCOL_RED, CONCOL_BLACK );
                wprintf( L"Error\n-- cannot load resource from %S\n", srclist[cnt] );
                wprintf( L"-- Patch failed !\n" );
                return;
            }
        }
     
        setconcol( CONCOL_BLUE, CONCOL_BLACK );
        wprintf( L"All patches completed.\n" );
        wprintf( L"Enjoy your updated PS CS4 11.0.2\n" );
    }
}

void printme()
{
    setconcol( CONCOL_WHITE );
    wprintf( L"==============================================================\n" );
    wprintf( L"= A Stand-alone Photoshop CS4 micro edition patch for 11.0.2 =\n" );
    wprintf( L"==============================================================\n" );
    wprintf( L"\n" );
    setconcol( CONCOL_YELLOW );
    wprintf( L" * This program built with lz4 library, version %s\n", LZ4_versionString() );
    wprintf( L" * LZ4 (https://lz4.github.io/lz4/) has copyright with LZ4 BSD2-CL.\n" );
    wprintf( L"\n" );
    setconcol( CONCOL_SILVER );
}

void cConsole()
{
    if ( AllocConsole() == TRUE )
    {
        hConsole = GetStdHandle( STD_OUTPUT_HANDLE );
        int hCrt = _open_osfhandle( (intptr_t) hConsole, _O_TEXT );
        hfCon = _wfdopen( hCrt, L"rw" );
        setvbuf( hfCon, NULL, _IONBF, 1 );
        *stdout = *hfCon;
    }
}

void eConsole()
{
    if ( hfCon != NULL )
    {
        fclose( hfCon );
        FreeConsole();
    }
}

int main( int argc, char** argv )
{
    cConsole();
    printme();
    loadProfiles();
    patchWork();

    setconcol( CONCOL_SILVER );
    wprintf( L"\n" );
    system("pause");

    eConsole();

    return 0;
}
